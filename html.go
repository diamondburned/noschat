package main

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

// Start launches the HTML Server
func Start() *HTMLServer {
	// Setup Context
	_, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Setup Handlers
	router := mux.NewRouter()
	router.HandleFunc("/", ChatPage)
	router.HandleFunc("/hist", HistoryPage)
	router.HandleFunc("/login", LoginPage)
	router.HandleFunc("/send", SendPage)
	router.HandleFunc("/set", SettingsHandler)

	router.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) {
		ServeCSS(w, r, StyleCSS)
	})
	router.HandleFunc("/dark.css", func(w http.ResponseWriter, r *http.Request) {
		ServeCSS(w, r, DarkCSS)
	})
	// router.PathPrefix("/assets/").Handler(
	// 	http.StripPrefix(
	// 		"/assets/",
	// 		http.FileServer(http.Dir("./assets")),
	// 	),
	// )

	// Create the HTML Server
	htmlServer := HTMLServer{
		server: &http.Server{
			Addr:           Addr,
			Handler:        router,
			ReadTimeout:    5000000000,
			WriteTimeout:   5000000000,
			MaxHeaderBytes: 1 << 20,
		},
	}

	// Add to the WaitGroup for the listener goroutine
	htmlServer.wg.Add(1)

	// Start the listener
	go func() {
		log.Printf("\nHTMLServer : Service started : Host=%v\n", Addr)
		htmlServer.server.ListenAndServe()
		htmlServer.wg.Done()
	}()

	return &htmlServer
}

// Stop turns off the HTML Server
func (htmlServer *HTMLServer) Stop() error {
	// Create a context to attempt a graceful 5 second shutdown.
	const timeout = 5 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	log.Printf("\nHTMLServer : Service stopping\n")

	// Attempt the graceful shutdown by closing the listener
	// and completing all inflight requests
	if err := htmlServer.server.Shutdown(ctx); err != nil {
		// Looks like we timed out on the graceful shutdown. Force close.
		if err := htmlServer.server.Close(); err != nil {
			log.Printf("\nHTMLServer : Service stopping : Error=%v\n", err)
			return err
		}
	}

	// Wait for the listener to report that it is closed.
	htmlServer.wg.Wait()
	log.Printf("\nHTMLServer : Stopped\n")
	return nil
}
