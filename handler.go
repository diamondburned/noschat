package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	uuid "github.com/satori/go.uuid"
)

// ChatServer is a struct for the template
type ChatServer struct {
	Name   string
	Unseen bool
}

// Settings contain user-specific and mainly UI settings
type Settings struct {
	// string as in "true" or "false"
	CompactMode string
	DarkMode    string
}

type CSSType string

const (
	StyleCSS CSSType = "style.css"
	DarkCSS  CSSType = "dark.css"
)

func MustUUID() string {
	UUID, err := uuid.NewV4()
	if err != nil {
		return "broken-uuid"
	}

	return UUID.String()
}

// LoginPage handles login
func LoginPage(wrt http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		settings := getSettings(req)

		fullData := map[string]interface{}{
			"CompactMode": settings.CompactMode,
			"DarkMode":    settings.DarkMode,
		}

		render(wrt, req, loginTpl, "loginTpl", fullData)
		return
	}

	if req.Method == "POST" {
		wrt.Header().Set("Cache-Control", "no-cache")

		var origin string

		userID, err := req.Cookie("uuid")
		if err != nil {
			log.Println("Generating new UUID")
			origin = MustUUID()
		} else {
			origin = userID.Value
		}

		http.SetCookie(wrt, &http.Cookie{
			Name:  "uuid",
			Value: origin,
		})

		nickname := req.FormValue("nickname")
		if nickname == "" {
			wrt.WriteHeader(403)
			wrt.Write([]byte("<h1>No username specified!</h1>"))

			return
		}

		if len(nickname) > cfg.NameLimit {
			wrt.WriteHeader(403)
			wrt.Write([]byte("<h1>Maximum username length reached!</h1>"))

			return
		}

		author := Author{
			Nickname: nickname,
			UUID:     origin,
			Expire:   time.Now().Add(time.Hour * 720),
		}

		authorJSON, err := json.Marshal(author)
		if err != nil {
			log.Println(err.Error())
			writeErr(wrt, err)
			return
		}

		if err := db.Put([]byte("uuid:"+origin), authorJSON, nil); err != nil {
			log.Println(err.Error())
			writeErr(wrt, err)
			return
		}

		log.Println("User", string(nickname), "joined")
		http.Redirect(wrt, req, "/", 301)
	}

	wrt.WriteHeader(403)
	wrt.Write([]byte("<h1>Invalid request type (GET/POST only)!</h1>"))
}

func writeErr(wrt http.ResponseWriter, err error) {
	log.Println(err.Error())
	wrt.WriteHeader(500)
	wrt.Write([]byte("<h1>" + err.Error() + "</h1>"))
}

// HistoryPage prints chat as history
func HistoryPage(wrt http.ResponseWriter, req *http.Request) {
	if req == nil {
		panic("WHAT THE FUCK")
	}

	author := handleUser(wrt, req)

	currentChannel := req.FormValue("channel")
	if currentChannel == "" {
		currentChannel = cfg.Servers[0]
	}

	if !checkChannels(currentChannel) {
		wrt.WriteHeader(403)
		wrt.Write([]byte("<h1>Server not found</h1>"))
		return
	}

	// chat := loadChat()

	msgs := PrettifyMessages(
		currentChannel,
		author.Nickname,
		false,
	)

	wrt.Header().Set("Cache-Control", "no-cache")

	settings := getSettings(req)

	fullData := map[string]interface{}{
		"CurrentChannel": currentChannel,
		"Messages":       msgs,
		"DarkMode":       settings.DarkMode,
	}

	render(wrt, req, histTpl, "histTpl", fullData)
}

// ChatPage handles actual chat
func ChatPage(wrt http.ResponseWriter, req *http.Request) {
	if req == nil {
		panic("WHAT THE FUCK")
	}

	author := handleUser(wrt, req)

	currentChannel := req.FormValue("channel")
	if currentChannel == "" {
		currentChannel = cfg.Servers[0]
	}

	if !checkChannels(currentChannel) {
		wrt.WriteHeader(403)
		wrt.Write([]byte("<h1>Server not found</h1>"))
		return
	}

	paused := false

	pausedVal := req.FormValue("paused")
	if pausedVal == "true" {
		paused = true
	}

	// chat := loadChat()

	var svs []ChatServer

	msgs := PrettifyMessages(
		currentChannel,
		author.Nickname,
		true,
	)

	for _, serverName := range cfg.Servers {
		svs = append(svs, ChatServer{
			Name: serverName,
			Unseen: func() bool {
				if len(msgs) < 1 {
					return false
				}

				if time.Now().Sub(msgs[0].Timestamp) <= 0 {
					return true
				}

				return false
			}(),
		})
	}

	wrt.Header().Set("Cache-Control", "no-cache")

	settings := getSettings(req)

	fullData := map[string]interface{}{
		"CurrentChannel": currentChannel,
		"Servers":        svs,
		"Messages":       msgs,
		"CompactMode":    settings.CompactMode,
		"DarkMode":       settings.DarkMode,
		"Paused":         paused,
	}

	render(wrt, req, chatTpl, "chatTpl", fullData)
}

// SendPage handles sending messages
func SendPage(wrt http.ResponseWriter, req *http.Request) {
	if req == nil {
		panic("WHAT THE FUCK")
	}

	author := handleUser(wrt, req)

	currentChannel := req.FormValue("channel")
	if currentChannel == "" {
		currentChannel = cfg.Servers[0]
	}

	if !checkChannels(currentChannel) {
		wrt.WriteHeader(403)
		wrt.Write([]byte("<h1>Server not found</h1>"))
		return
	}

	if req.Method == "GET" {
		settings := getSettings(req)

		wrt.Header().Set("Cache-Control", "no-cache")

		fullData := map[string]interface{}{
			"CurrentChannel": currentChannel,
			"CompactMode":    settings.CompactMode,
			"DarkMode":       settings.DarkMode,
		}

		render(wrt, req, sendTpl, "sendTpl", fullData)
		return
	}

	if req.Method == "POST" {
		content := req.FormValue("content")
		if content == "" {
			http.Redirect(wrt, req, "/", http.StatusMovedPermanently)

			return
		}

		if len(content) > cfg.CharLimit {
			wrt.WriteHeader(403)
			wrt.Header().Set("Content-Type", "text/plain")
			wrt.Write([]byte(fmt.Sprintf(
				"Maximum content length reached! (%d)\n\nYour content:\n%s",
				cfg.CharLimit,
				content,
			)))

			return
		}

		ctype := parseCType(req.FormValue("contenttype"))

		msg := Message{
			Author:      author,
			Timestamp:   time.Now(),
			Content:     content,
			ContentType: ctype,
		}

		err := SendMessage(currentChannel, msg)
		if err != nil {
			wrt.WriteHeader(403)
			wrt.Write([]byte("<h1>" + err.Error() + "</h1>"))
			return
		}

		http.Redirect(wrt, req, "/?channel="+currentChannel, 301)
	}

	wrt.WriteHeader(403)
	wrt.Write([]byte("<h1>Invalid request type (GET/POST only)!</h1>"))
}

// ServeCSS writes the CSS manually
func ServeCSS(wrt http.ResponseWriter, req *http.Request, ctype CSSType) {
	if req.Method != "GET" {
		wrt.WriteHeader(404)
	}

	// wrt.Header().Set("Cache-Control", "no-cache")
	wrt.Header().Set("Content-Type", "text/css")
	wrt.WriteHeader(200)

	switch ctype {
	case DarkCSS:
		wrt.Write(darkTpl)
	default:
		wrt.Write(cssTpl)
	}
}

// SettingsHandler handles a user's setting
func SettingsHandler(wrt http.ResponseWriter, req *http.Request) {
	switch req.FormValue("settings") {
	case "compact":
		cookieBool(wrt, req, "compact")
	case "dark":
		cookieBool(wrt, req, "dark")
	default:
		wrt.WriteHeader(404)
		wrt.Write([]byte("Missing settings"))
	}
}

// cookieBool flips the cookie setting
func cookieBool(wrt http.ResponseWriter, req *http.Request, key string) {
	ref := req.Header.Get("Referer")

	cookie, err := req.Cookie(key)
	if err != nil || cookie == nil {
		if err == http.ErrNoCookie {
			log.Println("Couldn't find cookie for key", key)
			http.SetCookie(wrt, &http.Cookie{
				Name:  key,
				Value: "true",
			})

			wrt.Header().Set("Cache-Control", "no-cache")
			http.Redirect(wrt, req, ref, 301)
			return
		}

		log.Println(err.Error())
		return
	}

	if cookie.Value == "true" {
		http.SetCookie(wrt, &http.Cookie{
			Name:  key,
			Value: "false",
		})
	} else {
		http.SetCookie(wrt, &http.Cookie{
			Name:  key,
			Value: "true",
		})
	}

	wrt.Header().Set("Cache-Control", "no-cache")
	http.Redirect(wrt, req, ref, 301)
}

func getSettings(req *http.Request) Settings {
	CompactMode := "false"
	cookie, err := req.Cookie("compact")
	if err == nil {
		if cookie.Value != "" {
			CompactMode = cookie.Value
		}
	}

	DarkMode := "false"
	cookie, err = req.Cookie("dark")
	if err == nil {
		if cookie.Value != "" {
			DarkMode = cookie.Value
		}
	}

	return Settings{
		CompactMode: CompactMode,
		DarkMode:    DarkMode,
	}
}

func handleUser(wrt http.ResponseWriter, req *http.Request) Author {
	var origin string

	var oldAuthor Author

	userid, err := req.Cookie("uuid")
	if err != nil {
		log.Println(err.Error())
		http.Redirect(wrt, req, "/login", http.StatusMovedPermanently)
		return oldAuthor
	}

	origin = userid.Value

	data, err := db.Get([]byte("uuid:"+origin), nil)
	if err != nil {
		log.Println(err.Error())
		http.Redirect(wrt, req, "/login", http.StatusMovedPermanently)
		return oldAuthor
	}

	if err := json.Unmarshal(data, &oldAuthor); err != nil {
		log.Println(err.Error())
		http.Redirect(wrt, req, "/login", http.StatusMovedPermanently)
		return oldAuthor
	}

	// Check if the info is expired
	if oldAuthor.Expire.Sub(time.Now()) < 0 {
		log.Println(err.Error())
		http.Redirect(wrt, req, "/login", http.StatusMovedPermanently)
		return oldAuthor
	}

	return oldAuthor
}

func checkChannels(currentChannel string) bool {
	for _, servers := range cfg.Servers {
		if currentChannel == servers {
			return true
		}
	}

	return false
}

// Render a template, or server error.
func render(w http.ResponseWriter, r *http.Request, tpl *template.Template, name string, data interface{}) {
	buf := new(bytes.Buffer)
	if err := tpl.ExecuteTemplate(buf, name, data); err != nil {
		fmt.Printf("\nRender Error: %v\n", err)
		return
	}
	w.Write(buf.Bytes())
}

// Push the given resource to the client.
func push(w http.ResponseWriter, resource string) {
	pusher, ok := w.(http.Pusher)
	if ok {
		if err := pusher.Push(resource, nil); err == nil {
			return
		}
	}
}
