package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"
)

// Config .
type Config struct {
	Servers []string `json:"servers"`
	// DatabasePath is the path to a directory
	DatabasePath string `json:"db_path"`
	CharLimit    int    `json:"char_limit"`
	RateLimit    string `json:"rate_limit"`
	NameLimit    int    `json:"name_limit"`
	NoDupe       bool   `json:"no_dupe"`
	ratelimit    time.Duration
}

type badgerConf struct {
	Dir      string `json:"directory"`
	ValueDir string `json:"value_directory"`
}

func defaultConfig() *Config {
	return &Config{
		Servers: []string{
			"general",
			"linux-masterrace",
			"shitpost",
		},
		DatabasePath: "./database",
		CharLimit:    512,
		RateLimit:    "5s",
		NameLimit:    14,
		NoDupe:       true,
	}
}

// newConfig loads the entire config file and do tests
func loadConfigs(path string) *Config {
	config := &Config{}

	data, err := ioutil.ReadFile(path)
	if err != nil {
		errorConfig()
		panic(err)
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		errorConfig()
		panic(err)
	}

	config.ratelimit, err = time.ParseDuration(config.RateLimit)
	if err != nil {
		errorConfig()
		panic(err)
	}

	if len(config.Servers) == 0 {
		errorConfig()
		panic("No servers in config")
	}

	return config
}

func errorConfig() {
	fmt.Println("Invalid config or config not found! Example config:")
	j, _ := json.MarshalIndent(defaultConfig(), "", "    ")
	fmt.Println(string(j))
}
