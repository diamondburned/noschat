package main

import (
	"encoding/json"
	"errors"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	ldbIter "github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/util"
	//"github.com/tidwall/buntdb"
)

// Chat map[ServerName]Messages
type Chat map[string][]Message

// Message represents each chat message
type Message struct {
	Author      Author
	Timestamp   time.Time
	Content     string
	ContentType ContentType
}

// Author is the author for each message
type Author struct {
	Nickname string
	UUID     string
	Expire   time.Time
}

// PrettyMessage is what is used to easily render
type PrettyMessage struct {
	Author          string
	Discriminator   string
	Content         string
	Timestamp       time.Time
	PrettyTimestamp string
	Mentioned       bool
	ContentType     ContentType
}

// ContentType is the type for a message
type ContentType string

const (
	// Code puts the message in a code class
	Code ContentType = "code/pre"

	// Image tries to embed the whole content as the image src
	Image ContentType = "img/src"

	// Text passes the content as normal
	Text ContentType = "text"
)

var (
	// ErrDuplicateMsg is returned when the latest message has duplicate content to prevent spam
	ErrDuplicateMsg = errors.New("Message has same content as the latest message")

	// ErrRateLimit is returned when the user spams messages too quickly
	ErrRateLimit = errors.New("Rate limit reached")

	// ErrUnknownIndex is returned when the message's iteration index couldn't be parsed properly
	ErrUnknownIndex = errors.New("Message's index can't be parsed. Check chat.go")
)

// SendMessage fetches the server, append the message and write it
//
// I am aware that this is a very costly solution, as the bigger
// the chat, the more it has to read and write. There are better
// ways to do this, such as using CSV files, but I'm using the
// database just as a proof of concept.
// Specs:
//     - Prefix for the database is "server:SERVERNAME:i"
//     - "i" is the index for the message
//     - "SERVERNAME" is the servername
func SendMessage(serverName string, msg Message) error {
	if len(cfg.Servers) < 1 {
		log.Fatalln("No servers declared")
	}

	msgentry := "server:" + serverName + ":0"

	iter := db.NewIterator(util.BytesPrefix([]byte("server:"+serverName+":")), nil)

	// Move to last entry
	iter.Last()

	if string(iter.Key()) != "" {
		msgentry = string(iter.Key())
		log.Println("GOT VALUE", string(iter.Value()), "FOR KEY msgentry")
	}

	var latestmsg Message
	if err := json.Unmarshal(iter.Value(), &latestmsg); err == nil {
		if time.Now().Sub(latestmsg.Timestamp) < cfg.ratelimit {
			return ErrRateLimit
		}

		if latestmsg.Content == msg.Content {
			return ErrDuplicateMsg
		}
	}

	// Free up
	iter.Release()
	if err := iter.Error(); err != nil {
		return err
	}

	bytes, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	keyParts := strings.Split(msgentry, ":")
	msgEntry, err := strconv.Atoi(keyParts[len(keyParts)-1])
	if err != nil {
		log.Println(msgentry, keyParts)
		spew.Dump(msg)
		return errors.New("Logic error at chat.go, part msgEntry")
	}

	msgint := strconv.Itoa(msgEntry + 1)

	log.Println("SETTING", "server:"+serverName+":"+msgint, "TO", string(bytes))

	if err := db.Put([]byte("server:"+serverName+":"+msgint), bytes, nil); err != nil {
		return err
	}

	return nil
}

// PrettifyMessages converts messages to web-friendly ones
func PrettifyMessages(currentChannel string, nickname string, limit bool) []PrettyMessage {
	var prettymsgs []PrettyMessage

	i := 0

	iter := db.NewIterator(util.BytesPrefix([]byte("server:"+currentChannel+":")), nil)

	defer func() {
		iter.Release()
		if err := iter.Error(); err != nil {
			log.Println(err.Error())
		}
	}()

	// Move to last
	iter.Last()

	// Shitty workaround for the loop skipping the first last entry
	pretty, err := fromIter(iter, nickname)
	if err != nil {
		log.Println(err.Error())
	} else {
		if pretty == nil {
			return prettymsgs
		}

		prettymsgs = append(prettymsgs, *pretty)

		// Iterate in reverse
		for iter.Prev() {
			pretty, err := fromIter(iter, nickname)
			if err != nil {
				log.Println(err.Error())
				continue
			}

			if pretty == nil {
				continue
			}

			prettymsgs = append(prettymsgs, *pretty)

			i++
			// if limit bool is true, stop
			if i > 50 && limit {
				break
			}
		}
	}

	if len(prettymsgs) < 1 {
		prettymsgs = append(prettymsgs, PrettyMessage{
			Author:          "Server",
			Discriminator:   "bot",
			Content:         "This channel is empty. Say something!",
			Timestamp:       time.Now(),
			PrettyTimestamp: time.Now().Format(time.Kitchen),
			Mentioned:       false,
			ContentType:     Text,
		})
	}

	return prettymsgs
}

func fromIter(iter ldbIter.Iterator, nickname string) (*PrettyMessage, error) {
	var msg Message

	if err := json.Unmarshal(iter.Value(), &msg); err != nil {
		log.Println(err.Error())
		return nil, err
	}

	mentioned := false
	if strings.Contains(msg.Content, "@"+nickname) {
		mentioned = true
	}

	return &PrettyMessage{
		Author:          msg.Author.Nickname,
		Discriminator:   strings.Split(msg.Author.UUID, "-")[0],
		Content:         msg.Content,
		Timestamp:       msg.Timestamp,
		PrettyTimestamp: msg.Timestamp.Format(time.Kitchen),
		Mentioned:       mentioned,
		ContentType:     msg.ContentType,
	}, nil
}

func parseCType(input string) ContentType {
	switch input {
	case string(Image):
		return Image
	case string(Code):
		return Code
	default:
		return Text
	}
}
