# noschat

Experimental JS-less chat

![Screenshot](https://gitlab.com/diamondburned/noschat/raw/master/.screenshot/screenshot.png)

## Some things

- Assets are packed in the binary
- Database optimizations are welcome (it's using BuntDB atm)
- Servers are defined in the config
- Unused servers are _not_ being dropped at the moment, so try and clean it yourself
- It is recommended you build this yourself, as assets are somehow not packed in CI builds

## Credits

BuntDB

## Example

#### There should be one running at https://chat.diamondb.xyz, but it might be dead