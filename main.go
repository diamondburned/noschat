package main

import (
	"flag"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"

	"github.com/gobuffalo/packr"
	"github.com/syndtr/goleveldb/leveldb"
)

var (
	Addr    string
	cfgPath string

	cfg *Config

	db *leveldb.DB

	chatTpl  *template.Template
	loginTpl *template.Template
	sendTpl  *template.Template
	histTpl  *template.Template

	// these 2 are actually a file
	cssTpl  []byte
	darkTpl []byte
)

// HTMLServer represents the web service that serves up HTML
type HTMLServer struct {
	server *http.Server
	wg     sync.WaitGroup
}

func main() {
	// Debugging purposes only
	log.SetFlags(log.Lshortfile)

	flag.StringVar(&Addr, "A", ":4307",
		"The port the webserver listens to")
	flag.StringVar(&cfgPath, "c", "./config.json",
		"The path to config file")

	flag.Parse()

	cfg = loadConfigs(cfgPath)

	var err error
	db, err = leveldb.OpenFile(cfg.DatabasePath, nil)
	if err != nil {
		panic(err)
	}

	// Load templats
	loadTpl()

	server := Start()

	defer func(server *HTMLServer) {
		server.Stop()
		db.Close()
	}(server)

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan

	log.Println("Shutting down")
}

func loadTpl() {
	box := packr.NewBox("./assets")

	cssTpl, _ = box.Find("style.css")
	darkTpl, _ = box.Find("dark.css")

	var (
		chat, _  = box.FindString("index.html")
		login, _ = box.FindString("login.html")
		send, _  = box.FindString("send.html")
		hist, _  = box.FindString("hist.html")
	)

	chatTpl = template.Must(template.New("chatTpl").Parse(chat))
	loginTpl = template.Must(template.New("loginTpl").Parse(login))
	sendTpl = template.Must(template.New("sendTpl").Parse(send))
	histTpl = template.Must(template.New("histTpl").Parse(hist))
}

func readFile(file string) string {
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		log.Panicln(err)
	}

	return string(bytes)
}
